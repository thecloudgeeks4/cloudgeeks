package Properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;


public class PropertiesHelperTest {
	private static final Logger logger = Logger.getLogger(PropertiesHelperTest.class);
	private final Properties configProp = new Properties();
	private final Properties apiEndPoints = new Properties();
	private final Properties testDataFile = new Properties();


	private PropertiesHelperTest() 
	{
		logger.debug("Read all properties from file");
		try {

			FileInputStream configPropFis = getFileInputStrem(
					System.getProperty("user.dir") + "/resources/config/Config.properties");
			if (configPropFis != null) 
			{
				configProp.load(configPropFis);
			}
			logger.debug("properties file load Done.");
			
			FileInputStream apiEndPointFis = getFileInputStrem(
					System.getProperty("user.dir") + "/resources/api/EndPoints.properties");
			if (apiEndPointFis != null) {
				apiEndPoints.load(apiEndPointFis);
			}
			FileInputStream testDataFis = getFileInputStrem(
					System.getProperty("user.dir") + "/resources/testData/testData.properties");
			if (testDataFile != null) {
				testDataFile.load(testDataFis);
			}
		} catch (IOException e) {
			logger.error("PropertiesHelper IOException:: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public Properties loadPropertyFile(String absolutepath) {
		Properties propFile = new Properties();
		try {
			FileInputStream propFileFis = new FileInputStream(absolutepath);
			propFile.load(propFileFis);

		} catch (Exception e) {
			logger.error("PropertiesHelper.loadPropertyFile Exception:: " + e.getMessage());
			e.printStackTrace();
		}
		return propFile;
	}

	// Bill Pugh Solution for singleton pattern
	private static class LazyHolder {
		private static final PropertiesHelperTest INSTANCE = new PropertiesHelperTest();
	}

	public static PropertiesHelperTest getInstance() {
		return LazyHolder.INSTANCE;
	}

	public String getConfigPropProperty(String key) {
		return configProp.getProperty(key);
	}

	public Set<String> getConfigPropAllPropertyNames() {
		return configProp.stringPropertyNames();
	}

	public boolean containsKeyFromConfigProp(String key) {
		return configProp.containsKey(key);
	}

	public String getEndpointProProperty(String key) {
		return apiEndPoints.getProperty(key);
	}
	
	public String getTestData(String key) {
		return testDataFile.getProperty(key);
	}


	public FileInputStream getFileInputStrem(String filePath) {
		FileInputStream fileInputStrem = null;
		try {
			fileInputStrem = new FileInputStream(filePath);
		} catch (Exception e) {
			logger.error("getFileInputStrem() exception msg::" + e.getMessage());
			logger.error("FILE NOT FOUND::" + filePath);
		}
		return fileInputStrem;
	}
	
}
