package ReusbaleComponents;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVWriter;

import Properties.PropertiesHelperTest;
import cucumber.api.DataTable;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class API_reusableComponents 
{
	
	private static final Logger logger = Logger.getLogger(API_reusableComponents.class);
	public static Response response;
	public static Headers reqHeaders;
	public static boolean blnRetainHeaders = false;
	public static PropertiesHelperTest loadProps = PropertiesHelperTest.getInstance();
	private String errorTxt;
	public static String apiEndpointIP;
	public static String apiEndpointWebURL;
	
	public API_reusableComponents() 
	{
		loadProps = PropertiesHelperTest.getInstance();
		apiEndpointIP = System.getProperty("apiEndpointIP");
		if (apiEndpointIP == null || apiEndpointIP.isEmpty()) 
		{
			apiEndpointIP = loadProps.getConfigPropProperty("api.baseURL");
		}
				logger.debug("API Endpoint IP Address:" + apiEndpointIP);
	}

	public boolean validateStatusCode(int expectedCode) {
		boolean flag = false;
		logger.debug("Actual:"+getStatusCode()+":: Expected:"+expectedCode);
		if(getStatusCode()==expectedCode){
			flag=true;
		}
		logger.debug("Validate Status Code FLAG::"+flag);
		return flag;
	}

	private String urlAddDebug(String url) {

		try {
			if (url != null) {
				if (url.contains("?")) {
					url = url;
				} else {
					url = url;
				}
			}
		} catch (Exception e) {
			logger.error("Exception Msg::" + e.getMessage());
		}
		logger.debug("EndPoint URL::" + url);
		return url;
	}

	public Response initiateRestAPICall(String url) {
		try {
			url = urlAddDebug(url);

			if (reqHeaders == null) {
				response = given().relaxedHTTPSValidation().when().get(url);
			} else {
				response = given().relaxedHTTPSValidation().headers(reqHeaders).when().get(url);
			}

			logger.debug("JSON Response::" + response.asString());
			if(blnRetainHeaders == false) {
				reqHeaders = null;
			}
		} catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::" + e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public static String prepareRestURIPathParam(String restURI, DataTable attributesTable, String newRecord) {
		String params = "?";
		String path = "";
		boolean handleParam = false;
		int i = 0;
		int startPos = 0;
		int endPos = 0;
		String whichSubString = null;

		if (attributesTable != null) {
			List<List<String>> attributes = attributesTable.raw();
			for (List<String> item : attributes) {
				if (i != 0) {
					if (item.get(0).equalsIgnoreCase("newrecord")) {
						params = "/" + newRecord;
						break;
					} else {
						handleParam = (item.get(2).toLowerCase().contains("param")) ? true : false;
							if (handleParam) {
								params = params + item.get(0) + "=" + item.get(1) + "&";
							} else {
								path = item.get(1);
							}
						//}
					}
					if (!handleParam) {
						if (restURI.contains("{")) {
							startPos = restURI.indexOf("{");
							endPos = restURI.indexOf("}", startPos);
							whichSubString = restURI.substring(startPos, endPos + 1);
							restURI = restURI.replace(whichSubString, path);
						}
					}
				}
				i++;
			}
		}

		if (params.length() > 0 && params != "?" && params.contains("&")) {
			if (params.contains("&")) {
				params = params.substring(0, (params.length() - 1));
			}
			if (params != "?") {
				restURI = restURI + params;
			}
		}

		return restURI;
	}
	
	public int getStatusCode() {
		return response.statusCode();
	}
	
	public Response getResponse() {
		return response;
	}

	public void writeNextFile(String scrum, String Sprint, String storyID, String fTPR)
	{
		File file = new File("resources/outPut/FTPRResult.csv"); 
	    try 
	    { 
        FileWriter outputfile = new FileWriter(file,true); 
	    
        CSVWriter writer = new CSVWriter(outputfile, ',', 
                                         CSVWriter.NO_QUOTE_CHARACTER, 
                                         CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
                                         CSVWriter.DEFAULT_LINE_END); 
  
        String[] data= {scrum,Sprint,storyID,fTPR};
        writer.writeNext(data);
    	writer.flush();
    	writer.close();
	}
	    catch (java.lang.NullPointerException e) 
        {
            throw new NullPointerException("The argument cannot be null"); 

        	//System.out.println("Null Pointer Exception");
        }
	catch (IOException e) 
	{
		e.printStackTrace();
	} 
	}
	
	public static String getFtprParsar(final String file) throws IOException 
	{
	 String FTPR="100%";
	 Object key = null;
	 int p3=0;
        try 
        {
			JSONParser parser = new JSONParser();
		
			JSONObject obj = (JSONObject) parser.parse(file);

			
            JSONObject jo = (JSONObject) obj; 
            JSONObject dataObj = (JSONObject) jo.get("fields");
            System.out.println(dataObj);

    		JSONArray jsonArray = (JSONArray) dataObj.get("issuelinks");
    		
    		for (Object projectObj : jsonArray.toArray()) 
			{
    			
    			JSONObject project = (JSONObject) projectObj;
	            JSONObject outwardIssue = (JSONObject) project.get("outwardIssue");
	            if(outwardIssue==null)
	            {
	            	
		           outwardIssue = (JSONObject) project.get("inwardIssue");
		           // System.out.println("inwardIssue-->>>>>" + outwardIssue);
		           System.out.println("inwardIssue-->>>>>");
	            }
	            else
	            {
		            System.out.println("outwardIssue-->>>>>");
		            //System.out.println("outwardIssue-->>>>>" + outwardIssue);

	            }
	            System.out.println(outwardIssue);
	         
	            key=outwardIssue.get("key");
	            System.out.println(key);


	            if(!key.equals("BUDP-6774"))
	            {
		            JSONObject fields = (JSONObject) outwardIssue.get("fields");
		            JSONObject priority = (JSONObject) fields.get("priority");		            
		            JSONObject issuetype = (JSONObject) fields.get("issuetype");
		            Object name=priority.get("name");
		            System.out.println(name);
		            Object nameIssue=issuetype.get("name");
		            System.out.println(nameIssue);
		            
		            if(name.equals("P3 - Major") && nameIssue.equals("Defect"))
		            {
		            	p3++;
		            }
		            if(p3>=5)
		            {
		            	FTPR="0%";
		            	FTPR=FTPR+",P3>5"; 
		            	break;
		            }
		            
		            if((name.equals("P1 - Blocker")||name.equals("P2 - Critical")) && nameIssue.equals("Defect"))
		            {
		            	FTPR="0%";
		            	FTPR=FTPR+","+key; 
		            	break;
		            }
	            }
		    
			}
  		
        }
        catch (java.lang.NullPointerException e) 
        {
            throw new NullPointerException("The argument cannot be null"); 
        }
		catch (ParseException e) 
		{
            throw new NullPointerException("The argument cannot be null"); 
		}
		return FTPR;
	
	}
	
	public void writeFile()
	{
		File file = new File("resources/outPut/FTPRResult.csv"); 
	    try 
	    { 
        FileWriter outputfile = new FileWriter(file,true); 
	    
        CSVWriter writer = new CSVWriter(outputfile, ',', 
                                         CSVWriter.NO_QUOTE_CHARACTER, 
                                         CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
                                         CSVWriter.DEFAULT_LINE_END); 
  
        List<String[]> data = new ArrayList<String[]>(); 
        data.add(new String[] { "Name", "Class", "Marks" }); 
        data.add(new String[] { "Aman", "10", "620" }); 
        data.add(new String[] { "Suraj", "10", "630" }); 
        writer.writeAll(data); 
        writer.close(); 
	}
	
	catch (IOException e) 
	{
		e.printStackTrace();
	} 
	}
	
	public static ArrayList<String> getStoryTCsParsar(final String file) throws IOException 
	{
		String TC_Details;
 		ArrayList <String>TC_inf = new ArrayList<String>();
 		Object key=null;
		Object createdBy=null ;
		Object lastTestResultStatus =null;
		Object objective=null;
		Object folder = null;
		Object iSRegression=null;
		Object iSAutomatable = null;
		Object priority=null;
		JSONObject project;
		JSONObject customFields;
		String resObjective;
		String des = null;

        try 
        {
	        // Object obj = new JSONParser().parse(new FileReader("files/StoriesTCs.json"));
        	// Object obj = new JSONParser().parse(new FileReader(file));
 				
	 			 Object obj = new JSONParser().parse(file);
	         JSONArray jsonArray = (JSONArray) obj;
    		
	         
    		for (Object projectObj : jsonArray.toArray()) 
			{
    			project = (JSONObject) projectObj;
    			customFields = (JSONObject) project.get("customFields");
    			try
    			{
    			
    			 key = project.get("key");
    			 createdBy = project.get("createdBy");
    			 objective = project.get("objective");
    			 folder = project.get("folder");
    			 priority=project.get("priority");
    			 lastTestResultStatus = project.get("lastTestResultStatus");
    			 iSRegression = customFields.get("Sanity/Regression");
    			 iSAutomatable = customFields.get("Execution Type");
    			 
    			 	if(iSRegression==null)
	    			{
	    				iSRegression="NO";
	    			}
	    			if(iSAutomatable==null)
	    			{
	    				iSAutomatable="MANUAL";
	    			}
    			
    			}		
    		    	 
    		    	 catch (NullPointerException e) 
    		        {
    		    		 System.out.println("There is a null field");
    		    		 //folder=null;
    		    		 e.getMessage();
    	    			 
    		        }
    			
    			if(!(objective==null))
    			{	
    				resObjective = (String) objective;
    				
    				des=resObjective.replaceAll(",","");
    			}
	            System.out.println(key);
	            System.out.println(createdBy);
	            System.out.println(lastTestResultStatus);
	            System.out.println(objective);
	            System.out.println(folder);
	            System.out.println(iSRegression);
	            System.out.println(iSAutomatable);
	            String viewType = (String) folder;
	            
	            if(!(viewType==null))
	            {
	            
		            if(viewType.contains("DSK"))
		            {
		            	viewType="Web";
		            }
		            else if( viewType.contains("MOB"))
		            {
		            	viewType="MOB";
		            }
		            else if(viewType.contains("API"))
		            {
		            	viewType="API";
		            }
		            else if(viewType.contains("SFMC"))
		            {
		            	viewType="SFMC";
		            }
		            else
		            {
		            	System.out.println(viewType + "exception with null");
		            }
	            }
	            
	            else
	            {
	            	viewType=null;
	            }
	            TC_Details=createdBy+","+des+","+key+","+priority+","+viewType+","+iSRegression+","+lastTestResultStatus+","+iSAutomatable;
	            TC_inf.add(TC_Details);
	            System.out.println(TC_Details);
	            
	            TC_Details=null;
	            
			}
			System.out.println(TC_inf.size());

    		for(String s1:TC_inf)
    		{
    			System.out.println(s1);
    		}
  		
        }
        catch (NullPointerException e) 
        {
            throw new NullPointerException("The argument cannot be null"); 

        }
		catch (ParseException e) 
			{
            throw new NullPointerException("The argument cannot be null"); 
			}
		return TC_inf;
	
	}

	public static String getStoryStatusDescriptionParser(final String file) throws IOException 
	{
	String getStoryStatusDescription = null;
	 JSONObject status = null;
	 Object statusStory = null;
	 Object summary = null;
	 String castSummary=null;
	 String respSummary=null;


	 int p3=0;
        try 
        {
        	
	       // Object obj = new JSONParser().parse(new FileReader("files/StoriesTCs.json"));
        	// Object obj = new JSONParser().parse(new FileReader(file));
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject) parser.parse(file);

		//	JSONObject obj = (JSONObject) parser.parse(new FileReader("files/StoryDetails.json"));

			
	        JSONObject jo = (JSONObject) obj; 
            JSONObject dataObj = (JSONObject) jo.get("fields");
            System.out.println(dataObj);
            summary=dataObj.get("summary");
            System.out.println(summary);
            status = (JSONObject) dataObj.get("status");
            System.out.println(status);
            statusStory=status.get("name");
            System.out.println(statusStory);
            
            castSummary = (String) summary;
            respSummary=castSummary.replaceAll(",","");
            
            
            getStoryStatusDescription=respSummary+","+statusStory;
            

            System.out.println(getStoryStatusDescription);
    		
        }
        catch (NullPointerException e) 
        {
            throw new NullPointerException("The argument cannot be null"); 

        }
		catch (ParseException e) 
		{
            throw new NullPointerException("The argument cannot be null"); 
		}
		return getStoryStatusDescription;
	
	}

}
