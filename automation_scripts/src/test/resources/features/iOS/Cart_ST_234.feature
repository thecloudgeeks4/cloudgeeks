@ST_234
Feature: iOS-App-SRetail customer verify mobile app in IOS 

Background:
    When "Retail User" sets the prerequisite setting to launch the iOS app


@ATM_TC001
  Scenario: Retail User calculate his payment price using mobile APP
    Given "Retail User" launchs the calculate app in "iOS"
    When "Retail User" sends inputs "for 1st Order & 2nd Order" in cart 
      | ElementType | InputValue|
      | input_textbox1         | 5000  |
      | input_textbox2         | 6000  |
    And "Retail User" clicks on "verify Calculate button" in cart
    Then "Retail User" verifies the "consolidated amount" in cart 
    And  "Retail User" closes the app
    
 