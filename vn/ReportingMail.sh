#!/bin/bash
# set the STRING variable
STRING="Sending the Report Mail"
echo $STRING
pwd
cd ../../../../
cd  C:/'Automation Projects'/cloudgeeks/target
pwd
tar.exe  -a  -c  -f cucumber-reports.zip cucumber-reports
cd ..
cd automation_scripts
pwd
mvn install -Dtestsuitnamefromci="**/*ReportingMailTestRunner.java"
echo "Mail Sent"